<?php
     define('PATH', realpath(__DIR__));
    require '../config.php';
    require PATH.'/dao/TblocalDaoMySql.php';
    require PATH.'/dao/CircuitoDaoSql.php';
    include PATH.'/header.php';

    $tblocalDao=new TblocalDaoMysql($pdo);
    $lista=$tblocalDao->findAll();
    $circuitoDao=new CircuitoDaoSql($pdo);
    $listaCircuito=$circuitoDao->findAll();

?>
<main>
    <section class="width:900px;">
        <div class="container" style="width:100%!important;min-width: 1200px">
            <div class="row" style="margin-top: 40px;">
                <div class="col s12 m10">
                    <a href="adicionar.php" class="btn">ADICIONAR LOCAL</a>
                    <a href="circuito/adicionar.php" class="btn">ADICIONAR CIRCUITOS</a>
                </div>
            </div>
            </div>
            <div class="container">
            <div class="row">
                <div class="col s12 m10">
                <hr style="background-color:rgb(230, 230, 230);">
                </div>
            </div>
        
            <br>
            <div class="row">
                <div class="col s12 m10 z-depth-2">
                    <table border="1" width="100%">
                        <tr>
                            <th>ID</th>
                            <th>LOCAL</th>
                            <th>AÇÔES</th>
                        </tr>
                        <?php
                    foreach($lista as $tblocalDao):?>
                        <tr>
                            <td><?=$tblocalDao->getId();?></td>
                            <td><?=$tblocalDao->getNome();?></td>
                           
                            <td>
                                <a href="editar.php?id=<?=$tblocalDao->getId();?>">[Editar]</a>
                                <a href="excluir.php?id=<?=$tblocalDao->getId();?>" onclick="return confirm('Tem certeza que deseja excluir?')">[Excluir]</a>
                                </td>
                        </tr>
                    <?php endforeach
                        ?>
                    </table>
                </div>
            </div>
        <br>
        <div class="row">
                <div class="col s12 m10 z-depth-2">
                    <table border="1" width="100%">
                        <tr>
                            <th>ID</th>
                            <th>CIRCUITO</th>
                            <th>ID LOCAL</th>
                            <th>AÇÔES</th>
                        </tr>
                        <?php
                    foreach($listaCircuito as $circuito):?>
                        <tr>
                            <td><?=$circuito->getId();?></td>
                            <td><?=$circuito->getCircuito();?></td>
                            <td><?=$circuito->getIdLocal();?></td>
                            <td>
                                <a href="/circuito/editar.php?id=<?=$circuito->getId();?>">[Editar]</a>
                                <a href="/circuito/excluir.php?id=<?=$circuito->getId();?>" onclick="return confirm('Tem certeza que deseja excluir?')">[Excluir]</a>
                                </td>
                        </tr>
                    <?php endforeach
                        ?>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m10">
                <hr style="background-color:rgb(230, 230, 230);">
                </div>
            </div>
            </div>
     
    </section>
       <!-- z-depth-2 fa fa-reply-->
</main>
<?php
    include PATH.'/footer.php';
?>