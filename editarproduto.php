<?php
require 'config.php';
require 'dao/ProdutosDaoMySQL.php';
include 'header.php';
$produtosDao=new ProdutosDaoMySQL($pdo);

$info=false;
$id = filter_input(INPUT_GET,'id');
if($id){
    $produtos=$produtosDao->findById($id);
}
if($produtos === false){
    header("Location: index.php");
    exit;
}
?>

<main class="white">
<section style="width:900px;margin:10px auto;">
<div class="container">
        <div class="row">
            <div class="col s12 m12">
                <h4>EDITAR PRODUTOS</h4>
            </div>
        </div>
</div>
        <div class="row">
            <div class="col s12 m10 ">
                <form method="POST" action="editar_action.php?id=2">
                    <div class="row">
                        <div class="col s12 m12 ">
                            <input type="text" name="id" value="<?=$produtos->getId();?>"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m12 ">
                        <label >
                            Nome: </br>
                            <input type="text" name="name" value="<?=$produtos->getNome();?>"/>
                        </label>
                        </div>

                        <div class="col s12 m6 ">
                            <label >
                                Quantidade: </br>
                                <input type="email" name="email" value="<?=$produtos->getQuantidade();?>"/>
                            </label>
                        </div>
                        <div class="col s12 m6 ">
                        <label for="">
                            Valor:
                        </label>
                            <input type="text" name="valor" value="<?=$produtos->getValor();?>">
                    </div>
                    <div class="row">
                        <div class="col s12 m6 ">
                        <label for="">
                            Categoria:
                        </label>
                        <input type="text" name="campo_categoria" value="<?=$produtos->getCampoCategoria();?>">
                        </div>
                    </div>
                <input class="btn" type="submit" value="Salvar"/>
                </form>
            </div>
        </div>
</div>
</section>
</main>
<?php
include 'footer.php';
?>