<?php
define('PATH', realpath(__DIR__.'../'));
require PATH.'/../config.php';
require PATH.'../dao/TblocalDaoMySql.php';
require PATH.'../dao/CircuitoDaoSql.php';

$tblocalDao = new TblocalDaoMySQL($pdo);
$tbcircuitoDao = new CircuitoDaoSql($pdo);

$nome = filter_input(INPUT_POST, 'nome');

if($nome){
    if($tblocalDao->findByNome($nome)=== false){
        $novoTblocal= new Tblocal();
        $novoTblocal->setNome($nome);

        $tblocalDao->add($novoTblocal);

        header("Location: index.php");
        exit;
    }else{
        header("Location: adicionar.php");
        exit;
    }
}else{
    header("Location: adicionar.php"); 


    if($circuito){
        if($tbcircuitoDao->findByCircuito($circuito)=== false){
            $novoTbcircuito= new Tblocal();
            $novoTbcircuito->setNome($circuito);
    
            $tbcircuitoDao->add($Circuito);
    
            header("Location: index.php");
            exit;
        }else{
            header("Location: adicionar_circuito.php");
            exit;
        }
    }else{
        header("Location: adicionar.php"); 
        exit;
}
}
