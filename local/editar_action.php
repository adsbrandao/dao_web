<?php
define('PATH', realpath(__DIR__.'../'));
require PATH.'/../config.php';
require PATH.'../dao/TblocalDaoMySql.php';

$tblocalDao= new TblocalDaoMySql($pdo);

$id= filter_input(INPUT_POST,'id');
$nome = filter_input(INPUT_POST, 'nome');

if($id && $nome){
    $tblocal=$tblocalDao->findById($id);
    $tblocal->setNome($nome);


    $tblocalDao->update($tblocal);


    header("Location: index.php");
    exit;
}else{
    header("Location: editar.php?id=".$id);
    exit;
}

