<?php

require_once PATH."/models/Circuito.php";

class CircuitoDaoSql implements CircuitoDao{
    private $pdo;
    public function __construct(PDO $driver)
    {
        $this->pdo=$driver;
    }
    public function add(Circuito $c){
        $sql=$this->pdo->prepare("INSERT INTO circuito(circuito,id_local)VALUES(:circuito,:id_local)");
        $sql->bindValue(':id',$c->getId());
        $sql->bindValue(':circuito',$c->getCircuito());
        $sql->bindValue(':id_local',$c->getIdLocal());
        $sql->execute();

        $c->setId( $this->pdo->lastInsertId() );
        return $c;
    }
    public function update(Circuito $c){
        $sql=$this->pdo->prepare("UPDATE circuito SET circuito=:circuito,id_local=:id_local WHERE id=:id");
        $sql->bindValue(':id',$c->getId());
        $sql->bindValue(':circuito',$c->getCircuito());
        $sql->bindValue(':id_local',$c->getIdLocal());
        $sql->execute();

        return true;
    }
    public function delete($id){
        $sql=$this->pdo->prepare("DELETE FROM circuito WHERE id=:id");
        $sql->bindValue(':id',$id);
        $sql->execute();
    }
    public function findAll(){
        $array=[];
        $sql=$this->pdo->query("SELECT * FROM circuito");
        if($sql->rowCount() > 0){
            $data=$sql->fetchAll();

            foreach($data as $item){
               $c = new Circuito();
               $c->setId($item['id']);
               $c->setCircuito($item['circuito']);
               $c->setIdLocal($item['id_local']);

                $array[] =$c;
            }
        }
        return $array;
    }
    public function findById($id){
        $sql=$this->pdo->prepare("SELECT * FROM circuito WHERE id =:id");
        $sql->bindValue(':id',$id);
        $sql->execute();
        if($sql->rowCount() > 0){
            $data=$sql->fetch();

            $c = new Circuito();
            $c->setId($data['id']);
            $c->setCircuito($data['circuito']);
            $c->setIdLocal($data['id_local']);

            return $c;
        }else{
            return false;
        }
    }
    public function findByCircuito($circuito){
        $sql=$this->pdo->prepare("SELECT * FROM circuito WHERE circuito =:circuito");
        $sql->bindValue(':circuito',$circuito);
        $sql->execute();
        if($sql->rowCount() > 0){
            $data=$sql->fetch();

            $c = new Circuito();
            $c->setId($data['id']);
            $c->setCircuito($data['circuito']);
            $c->setIdLocal($data['id_local']);

            return $c;
        }else{
            return false;
        }
    }
}
?>