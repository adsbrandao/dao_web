<?php
require_once PATH.'/models/Tblocal.php';

class  TblocalDaoMySql implements TblocalDao{
    private $pdo;
    public function __construct(PDO $driver)
    {
        $this->pdo=$driver;
    }
    public function add(Tblocal $t){
        $sql=$this->pdo->prepare("INSERT INTO tblocal(nome) VALUES(:nome)");
        $sql->bindValue(':nome',$t->getNome());
        $sql->execute();

        $t->setId( $this->pdo->lastInsertId() );
        return $t;
    }
    public function update(Tblocal $t){
        $sql=$this->pdo->prepare("UPDATE tblocal SET nome=:nome WHERE id=:id");
        $sql->bindValue(':nome',$t->getNome());
        $sql->bindValue(':id',$t->getId());
        $sql->execute();

        return true;
    }
    public function delete($id){
        $sql=$this->pdo->prepare("DELETE FROM tblocal WHERE id=:id");
        $sql->bindValue(':id',$id);
        $sql->execute();
    }
    public function findAll(){
        $array=[];
        $sql=$this->pdo->query("SELECT * FROM tblocal");
        if($sql->rowCount() > 0){
            $data=$sql->fetchAll();

            foreach($data as $item){
                $t = new Tblocal();
                $t->setId($item['id']);
                $t->setNome($item['nome']);
              
                $array[] = $t;
            }
        }
        return $array;
    }
    public function findById($id){
        $sql=$this->pdo->prepare("SELECT * FROM tblocal WHERE id =:id");
        $sql->bindValue(':id',$id);
        $sql->execute();
        if($sql->rowCount() > 0){
            $data=$sql->fetch();

            $t = new Tblocal();
            $t->setId($data['id']);
            $t->setNome($data['nome']);

            return $t;
        }else{
            return false;
        }
    }
    public function findByNome($nome){
        $sql=$this->pdo->prepare("SELECT * FROM tblocal WHERE nome =:nome");
        $sql->bindValue(':nome',$nome);
        $sql->execute();
        if($sql->rowCount() > 0){
            $data=$sql->fetch();

            $t = new Tblocal();
            $t->setId($data['id']);
            $t->setNome($data['nome']);

            return $t;
        }else{
            return false;
        }
    }
}
?>