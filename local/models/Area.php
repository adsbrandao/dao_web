<?php

class Area
{
    
    private $id;
    private $area;
    private $id_circuito;

    public function getId(){
        return $this->id;
    }
    public function setId($i){
        $this->id=trim($i);
    }
    public function getArea(){
        return $this->area;
    }
    public function setArea($i){
        $this->area=trim($i);
    }
    public function getIdCircuito(){
        return $this->id_circuito;
    }
    public function setIdCircuito($i){
        $this->id_circuito=trim($i);
    }
}
interface AreaDao{
    public function add(Produtos $p);
    public function update(Produtos $p);
    public function delete($id);
    public function findAll();
    public function findById($id);
    public function findByCircuito($Circuito);
}
?>