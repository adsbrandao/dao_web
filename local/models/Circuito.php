<?php

class Circuito
{
    
    private $id;
    private $circuito;
    private $id_local;

    public function getId(){
        return $this->id;
    }
    public function setId($i){
        $this->id=trim($i);
    }
    public function getCircuito(){
        return $this->circuito;
    }
    public function setCircuito($i){
        $this->circuito=trim($i);
    }
    public function getIdLocal(){
        return $this->id_local;
    }
    public function setIdLocal($i){
        $this->id_local=trim($i);
    }
}
interface CircuitoDao{
    public function add(Circuito $c);
    public function update(Circuito $c);
    public function delete($id);
    public function findAll();
    public function findById($id);
    public function findByCircuito($Circuito);
}
?>