<?php

class Tblocal
{
    
    private $id;
    private $nome;

    public function getId(){
        return $this->id;
    }
    public function setId($i){
        $this->id=trim($i);
    }
    public function getNome(){
        return $this->nome;
    }
    public function setNome($i){
        $this->nome=trim($i);
    }
}
interface TblocalDao{
    public function add(Tblocal $l);
    public function update(Tblocal $l);
    public function delete($id);
    public function findAll();
    public function findById($id);
    public function findByNome($nome);
}
?>

