<?php
define('PATH', realpath(__DIR__.'../'));
require PATH.'/../config.php';
require PATH.'../dao/CircuitoDaoMySql.php';

$circuitoDao= new CircuitoDaoMySQL($pdo);

$nome = filter_input(INPUT_POST, 'nome');

if($nome){
    if($circuitoDao->findByNome($nome)=== false){
        $novoCircuito= new Circuito();
        $novoCircuito->setNome($nome);


        $circuitoDao->add($novoCircuito);

        header("Location: ../index.php");
        exit;
    }else{
        header("Location: adicionar.php");
        exit;
    }
}else{
    header("Location: adicionar.php"); 
    exit;
}
