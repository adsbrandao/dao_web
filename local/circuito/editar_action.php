<?
define('PATH', realpath(__DIR__.'/../'));
require PATH.'/../config.php';
require PATH.'../dao/LocalDaoMysql.php';

$localDao = new LocalDaoMySQL($pdo);

$id= filter_input(INPUT_POST,'id');
$name = filter_input(INPUT_POST, 'name');
$email = filter_input(INPUT_POST,'email');
$endereco = filter_input(INPUT_POST,'endereco');

if($id && $name && $email){
    $local=$localDao->findById($id);
    $local->setLocal($local);
    $local->setId($id);

    $localDao->update($local);

    header("location: ../index.php");
    exit;
}else{
    header("Location: ../editar.php?id=".$id); 
    exit;
}
